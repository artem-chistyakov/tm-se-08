package ru.chistyakov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.entity.Task;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;


public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public boolean insert(@NotNull final String userId, @NotNull final String projectId, @NotNull final String taskName, @Nullable final String descriptionTask, @Nullable final Date dateBeginTask, @Nullable final Date dateEndTask) {
        Task newTask = new Task();
        if (entities.containsKey(newTask.getId())) return false;
        newTask.setUserId(userId);
        newTask.setName(taskName);
        newTask.setProjectId(projectId);
        newTask.setDescription(descriptionTask);
        newTask.setDateBeginTask(dateBeginTask);
        newTask.setDateEndTask(dateEndTask);
        entities.put(newTask.getId(), newTask);
        return true;
    }

    @Override
    public boolean persist(@NotNull final Task task) {
        if (task.getUserId() == null || task.getProjectId() == null) return false;
        return insert(task.getUserId(), task.getProjectId(), task.getName(), task.getDescription(), task.getDateBeginTask(), task.getDateEndTask());

    }

    @Override
    @Nullable
    public Task findOne(@NotNull final String userId, @NotNull final String taskId) {
        return findById(userId, taskId);
    }

    @Override
    @NotNull
    public Collection<Task> findAllByProjectId(@NotNull final String projectId) {
        final Collection<Task> taskCollection = new ArrayList<>();
        for (final Task task : entities.values()) if (projectId.equals(task.getProjectId())) taskCollection.add(task);
        return taskCollection;
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String taskId, @NotNull final String projectId,
                          @NotNull final String name, @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        final Task task = findById(userId, taskId);
        if (task == null) return false;
        if (!userId.equals(task.getUserId())) return false;
        task.setProjectId(projectId);
        task.setName(name);
        task.setDateEndTask(dateEnd);
        task.setDateBeginTask(dateBegin);
        task.setDescription(description);
        return true;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) {
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) {
                entities.remove(taskId);
                return true;
            }
        return false;
    }

    @Override
    public boolean removeByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final List<String> listTaskId = findTaskIdByProjectId(userId, projectId);
        if (listTaskId.isEmpty()) return true;
        for (final String str : listTaskId) entities.remove(str);
        return true;
    }

    @Nullable
    private Task findById(@NotNull final String userId, @NotNull final String taskId) {
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId()) && taskId.equals(task.getId())) return task;
        return null;
    }

    @NotNull
    private List<String> findTaskIdByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        final List<String> tasksIdForFind = new ArrayList<>();
        for (final Task task : entities.values())
            if (projectId.equals(task.getProjectId()) && userId.equals(task.getUserId()))
                tasksIdForFind.add(task.getId());
        return tasksIdForFind;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        for (final Task task : entities.values()) if (userId.equals(task.getUserId())) entities.remove(task.getId());
    }

    @Override
    @NotNull
    public Collection<Task> findAll(@NotNull String userId) {
        final Collection<Task> collection = new ArrayList<>();
        for (final Task task : entities.values())
            if (userId.equals(task.getUserId())) collection.add(task);
        return collection;
    }

    @Override
    public boolean merge(@NotNull final Task task) {
        if (task.getUserId() == null || task.getProjectId() == null) return false;
        if (entities.get(task.getId()) == null)
            insert(task.getUserId(), task.getProjectId(), task.getName(), task.getDescription(), task.getDateBeginTask(), task.getDateEndTask());
        else
            update(task.getUserId(), task.getId(), task.getProjectId(), task.getName(), task.getDescription(), task.getDateBeginTask(), task.getDateEndTask());
        return false;
    }
}
