package ru.chistyakov.tm.repository;

import java.util.HashMap;
import java.util.Map;

public abstract class AbstractRepository<T> {

    final Map<String, T> entities = new HashMap<>();

    public abstract boolean persist(T t);

    public abstract boolean merge(T t);

}
