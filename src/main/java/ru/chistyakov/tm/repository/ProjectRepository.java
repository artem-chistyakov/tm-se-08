package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {


    @Override
    public boolean insert(@NotNull final String userId, @NotNull final String name, @Nullable final String description,
                          @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        final Project project = new Project();
        if (entities.containsKey(project.getId())) return false;
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        entities.put(project.getId(), project);
        return true;
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId()) && projectId.equals(project.getId())) return project;
        return null;
    }

    @Override
    @NotNull
    public Collection<Project> findAll(@NotNull final String userId) {
        final Collection<Project> projectCollection = new ArrayList<>();
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId())) projectCollection.add(project);
        return projectCollection;
    }

    @Override
    public boolean update(@NotNull final String projectId, @NotNull final String projectName,
                          @Nullable final String description, @Nullable final Date dateBegin, @Nullable final Date dateEnd) {
        if (!entities.containsKey(projectId)) return false;
        final Project project = entities.get(projectId);
        project.setName(projectName);
        project.setDescription(description);
        project.setDateBeginProject(dateBegin);
        project.setDateEndProject(dateEnd);
        return true;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId()) || projectId.equals(project.getId())) {
                entities.remove(projectId);
                return true;
            }
        return false;
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        for (final Project project : entities.values())
            if (userId.equals(project.getUserId())) entities.remove(project.getId());
    }

    @Override
    public boolean persist(@NotNull final Project project) {
        if (project.getUserId() == null) return false;
        return insert(project.getUserId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());
    }

    @Override
    public boolean merge(@NotNull final Project project) {
        if (project.getUserId() == null) return false;
        if (entities.get(project.getId()) == null)
            insert(project.getUserId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());
        else
            update(project.getId(), project.getName(), project.getDescription(), project.getDateBeginProject(), project.getDateEndProject());
        return false;
    }
}
