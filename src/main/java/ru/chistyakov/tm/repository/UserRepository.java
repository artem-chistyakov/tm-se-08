package ru.chistyakov.tm.repository;


import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IUserRepository;
import ru.chistyakov.tm.entity.User;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.ArrayList;
import java.util.Collection;


public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public boolean insert(@NotNull final String login, @NotNull final String password) {
        final User user = new User();
        if (entities.containsKey(user.getId())) return false;
        user.setRoleType(RoleType.USUAL_USER);
        user.setLogin(login);
        user.setPassword(password);
        entities.put(user.getId(), user);
        return true;
    }

    @Override
    @Nullable
    public User findUserByLoginAndPassword(@NotNull final String login, @NotNull final String passwordMD5) {
        final Collection<User> users = findByLogin(login);
        for (final User user : users) if (user.getPassword().equals(passwordMD5)) return user;
        return null;
    }

    @Override
    public boolean insertAdmin(@NotNull final String login, @NotNull final String password) {
        final User user = new User();
        if (entities.containsKey(user.getId())) return false;
        user.setRoleType(RoleType.ADMINISTRATOR);
        user.setLogin(login);
        user.setPassword(password);
        entities.put(user.getId(), user);
        return true;
    }

    @Override
    @Nullable
    public User findOne(@NotNull final String id) {
        return entities.get(id);
    }

    @Override
    @NotNull
    public Collection<User> findByLogin(@NotNull final String userLogin) {
        final Collection<User> collection = new ArrayList<>();
        for (final User user : entities.values()) if (user.getLogin().equals(userLogin)) collection.add(user);
        return collection;
    }

    @Override
    public boolean remove(@NotNull final String id) {
        return entities.remove(id) != null;
    }

    @Override
    public boolean persist(@NotNull final User user) {
        return insert(user.getLogin(), user.getPassword());
    }

    @Override
    public boolean merge(@NotNull final User user) {
        if (entities.get(user.getId()) == null)
            insert(user.getLogin(), user.getPassword());
        else
            update(user.getId(), user.getLogin(), user.getPassword());
        return false;
    }

    @Override
    public boolean update(@NotNull final String id, @NotNull final String login, @NotNull final String password) {
        final User user = entities.get(id);
        if (user == null) return false;
        user.setLogin(login);
        user.setPassword(password);
        return true;
    }
}
