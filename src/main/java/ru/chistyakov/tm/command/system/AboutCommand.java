package ru.chistyakov.tm.command.system;

import com.jcabi.manifests.Manifests;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;



public final class AboutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Информация о сборке приложения";
    }

    @Override
    public void execute() {
        System.out.println( "Версия приложения :"  +Manifests.read("Version") +"\n"+
                "разработчик :" + Manifests.read("Developer") +"\n"+
                "build number :" + Manifests.read("BuildNumber"));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ANONIM,RoleType.USUAL_USER,RoleType.ADMINISTRATOR};
    }
}
