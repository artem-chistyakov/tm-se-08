package ru.chistyakov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class HelpCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Выводит описание всех комманд";
    }

    @Override
    public void execute() {
        for (final AbstractCommand abstractCommand : serviceLocator.getCommandMap().values())
            System.out.println(abstractCommand.getName() + " : " + abstractCommand.getDescription());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ANONIM,RoleType.ADMINISTRATOR,RoleType.USUAL_USER};
    }
}
