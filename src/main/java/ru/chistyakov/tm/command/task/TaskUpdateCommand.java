package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class TaskUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tuc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет команду";
    }

    @Override
    public void execute() {
        System.out.println("Введите id пользователя");
        final String userId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите id задачи");
        final String taskId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое название задачи");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новое описание задачи");
        final String descriptionTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новую дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите новую дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskService().update(userId, taskId, projectId, taskName, descriptionTask, dateBeginTask, dateEndTask))
            System.out.println("Задача обновлена успешно");
        else System.out.println("Ошибка в обновлении задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
