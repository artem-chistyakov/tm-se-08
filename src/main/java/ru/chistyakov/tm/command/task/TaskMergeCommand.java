package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;


public class TaskMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если задачи не существует создает новую, иначе её обновляет";
    }

    @Override
    public void execute() {
        System.out.println("Введите id задачи");
        final String taskId = serviceLocator.getScanner().nextLine();
        final Task task = serviceLocator.getTaskService().findOne(serviceLocator.getCurrentUser().getId(), taskId);
        serviceLocator.getTaskService().merge(task);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
