package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class TaskRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "trc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление одной команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        System.out.println("Введите id задачи");
        final String taskId = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskService().remove(serviceLocator.getCurrentUser().getId(), taskId))
            System.out.println("Задача удалена");
        else System.out.println("Ошибка удаления задачи");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
