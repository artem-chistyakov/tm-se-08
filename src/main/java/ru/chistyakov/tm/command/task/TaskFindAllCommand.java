package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Task;
import ru.chistyakov.tm.enumerate.RoleType;

public final class TaskFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tfal";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Найти все команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        for (final Task task : serviceLocator.getTaskService().findAll(serviceLocator.getCurrentUser().getId()))
            System.out.println(task);
    }


    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
