package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class TaskInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tpc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создает новую задачу";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите название задачи");
        final String taskName = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание задачи");
        final String descriptionTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала задачи DD.MM.yyyy");
        final String dateBeginTask = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания задачи DD.MM.yyyy");
        final String dateEndTask = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getTaskService().insert(serviceLocator.getCurrentUser().getId(), projectId, taskName, descriptionTask, dateBeginTask, dateEndTask))
            System.out.println("Новая задача создана");
        else System.out.println("Ошибка вставки команды");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
