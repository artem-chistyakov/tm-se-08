package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class TaskRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "trac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаление всех команд авторизованного пользователя";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskService().removeAll(serviceLocator.getCurrentUser().getId());
        System.out.println("Все задачи удалены");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
