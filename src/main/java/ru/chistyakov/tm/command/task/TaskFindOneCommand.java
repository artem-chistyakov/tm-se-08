package ru.chistyakov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public class TaskFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "tfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одной команды авторизованного пользователя";
    }

    @Override
    public void execute() {
        System.out.println("Введите id задачи");
        System.out.println(serviceLocator.getTaskService().findOne(serviceLocator.getCurrentUser().getId(), serviceLocator.getScanner().nextLine()));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};

    }
}
