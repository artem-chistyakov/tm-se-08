package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class ProjectUpdateCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "puc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Обновляет проект";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("Введите название проекта");
        final String loginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала проекта DD.MM.yyyy");
        final String dateBeginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания проекта DD.MM.yyyy");
        final String dateEndProject = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectService().update(projectId, loginProject, descriptionProject, dateBeginProject, dateEndProject))
            System.out.println("Проект обновлен");
        else System.out.println("Ошилка обновления");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
