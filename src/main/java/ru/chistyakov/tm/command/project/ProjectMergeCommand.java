package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

public class ProjectMergeCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pmc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Если проекта не существует создает новый, иначе его обновляет";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        final Project project = serviceLocator.getProjectService().findOne(serviceLocator.getCurrentUser().getId(), projectId);
        serviceLocator.getProjectService().merge(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
