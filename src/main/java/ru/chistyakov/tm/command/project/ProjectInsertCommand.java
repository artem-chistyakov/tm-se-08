package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class ProjectInsertCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pic";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Создание нового проекта";
    }

    @Override
    public void execute() {
        System.out.println("Введите название проекта");
        final String loginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите описание проекта");
        final String descriptionProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату начала проекта DD.MM.yyyy");
        final String dateBeginProject = serviceLocator.getScanner().nextLine();
        System.out.println("Введите дату окончания проекта DD.MM.yyyy");
        final String dateEndProject = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectService().insert(serviceLocator.getCurrentUser().getId(), loginProject, descriptionProject, dateBeginProject, dateEndProject))
            System.out.println("Новый проект успешно создан");
        else System.out.println("Ошибка вставки проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
