package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public class ProjectRemoveAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "prac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет все проекты текущего пользователя";
    }

    @Override
    public void execute() {
        serviceLocator.getProjectService().removeAll(serviceLocator.getCurrentUser().getId());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
