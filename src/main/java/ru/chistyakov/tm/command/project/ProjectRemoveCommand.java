package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class ProjectRemoveCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "prc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Удаляет проект по id";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectService().remove(serviceLocator.getCurrentUser().getId(), projectId))
            System.out.println("Проект с таким id удален");
        else System.out.println("ошибка удаления проекта");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
