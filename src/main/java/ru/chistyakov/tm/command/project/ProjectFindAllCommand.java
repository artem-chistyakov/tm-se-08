package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.enumerate.RoleType;

import java.util.Collection;

public final class ProjectFindAllCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pfac";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск всех проектов авторизованного пользователя";
    }

    @Override
    public void execute() {
        final Collection<Project> projectCollection = serviceLocator.getProjectService().findAll(serviceLocator.getCurrentUser().getId());
        for (final Project project : projectCollection) System.out.println(project);
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
