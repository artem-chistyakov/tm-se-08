package ru.chistyakov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public class ProjectFindOneCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "pfoc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Поиск одного проекта по id у авторизованного пользователя";
    }

    @Override
    public void execute() {
        System.out.println("Введите id проекта");
        final String projectId = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getProjectService().findOne(serviceLocator.getCurrentUser().getId(), projectId) == null)
            System.out.println("Проект не найден");
        else
            System.out.println(serviceLocator.getProjectService().findOne(serviceLocator.getCurrentUser().getId(), projectId));
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};
    }
}
