package ru.chistyakov.tm.command;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.api.ServiceLocator;
import ru.chistyakov.tm.enumerate.RoleType;

public abstract class AbstractCommand {

    protected ServiceLocator serviceLocator;

    public ServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    public abstract String getName();

    @NotNull
    public abstract String getDescription();

    public abstract void execute();

    @NotNull
    public abstract RoleType[] getSupportedRoles();
}
