package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public class UserLookProfileCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "ulpc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Просмотр профиля пользователя";
    }

    @Override
    public void execute() {
        System.out.println(serviceLocator.getCurrentUser());
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.USUAL_USER, RoleType.ADMINISTRATOR};

    }
}
