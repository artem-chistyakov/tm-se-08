package ru.chistyakov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.command.AbstractCommand;
import ru.chistyakov.tm.enumerate.RoleType;

public final class UserRegistrationCommand extends AbstractCommand {

    @NotNull
    @Override
    public String getName() {
        return "urc";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "регистрация пользователя";
    }

    @Override
    public void execute() {
        System.out.println("введите имя");
        final String login = serviceLocator.getScanner().nextLine();
        System.out.println("введите пароль");
        final String password = serviceLocator.getScanner().nextLine();
        if (serviceLocator.getUserService().registryUser(login, password))
            System.out.println("Пользователь зарегистрирован");
        else System.out.println("Ошибка регистрации пользователя");
    }

    @NotNull
    @Override
    public RoleType[] getSupportedRoles() {
        return new RoleType[]{RoleType.ANONIM};
    }
}
