package ru.chistyakov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.api.IProjectRepository;
import ru.chistyakov.tm.api.IProjectService;
import ru.chistyakov.tm.api.ITaskRepository;
import ru.chistyakov.tm.entity.Project;
import ru.chistyakov.tm.utility.DateParser;

import java.util.Collection;

public final class ProjectService extends AbstractService<Project> implements IProjectService {

    private final IProjectRepository projectRepository;
    private final ITaskRepository taskRepository;

    public ProjectService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public boolean merge(@Nullable final Project project) {
        if (project == null) return false;
        return projectRepository.merge(project);
    }

    @Override
    public boolean persist(@NotNull final Project project) {
        return projectRepository.persist(project);
    }


    @Override
    public boolean insert(@NotNull final String userId, @NotNull final String login,
                          @Nullable String descriptionProject, @Nullable String dateBeginProject,
                          @Nullable String dateEndProject) {
        if (login.trim().isEmpty() || userId.trim().isEmpty()) return false;
        return projectRepository.insert(
                userId, login, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBeginProject),
                DateParser.parseDate(simpleDateFormat, dateEndProject));
    }

    @Override
    public void removeAll(@NotNull final String userId) {
        if (userId.trim().isEmpty()) return;
        projectRepository.removeAll(userId);
        taskRepository.removeAll(userId);
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String projectId) {
        if (projectId.trim().isEmpty()) return false;
        return projectRepository.remove(userId, projectId) && taskRepository.removeByProjectId(userId, projectId);
    }

    @Override
    @Nullable
    public Project findOne(@NotNull final String userId, @NotNull final String projectId) {
        return projectRepository.findOne(projectId, userId);
    }


    @Override
    public boolean update(@NotNull final String projectId, @NotNull final String projectNameProject,
                          @Nullable final String descriptionProject, @Nullable final String dateBegin,
                          @Nullable final String dateEnd) {
        if (projectId.trim().isEmpty() || projectNameProject.trim().isEmpty()) return false;
        return projectRepository.update(projectId, projectNameProject, descriptionProject,
                DateParser.parseDate(simpleDateFormat, dateBegin),
                DateParser.parseDate(simpleDateFormat, dateEnd));
    }

    @Override
    @NotNull
    public Collection<Project> findAll(@NotNull final String userId) {
        return projectRepository.findAll(userId);
    }
}
