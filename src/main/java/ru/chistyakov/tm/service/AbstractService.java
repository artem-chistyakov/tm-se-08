package ru.chistyakov.tm.service;

import java.text.SimpleDateFormat;

public abstract class AbstractService<T> {

    final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("DD.MM.yyyy");

    public abstract boolean merge(T t);

    public abstract boolean persist(T t);
}
