package ru.chistyakov.tm;

import org.apache.log4j.BasicConfigurator;
import ru.chistyakov.tm.command.project.*;
import ru.chistyakov.tm.command.system.AboutCommand;
import ru.chistyakov.tm.command.system.ExitCommand;
import ru.chistyakov.tm.command.system.HelpCommand;
import ru.chistyakov.tm.command.task.*;
import ru.chistyakov.tm.command.user.*;
import ru.chistyakov.tm.loader.Bootstrap;

public final class App {

    private final static Class[] CLASSES = {
            AboutCommand.class,
            UserAuthorizationCommand.class, UserRegistrationCommand.class, UserEndSessionCommand.class, UserEditProfileCommand.class,
            UserLookProfileCommand.class, UserUpdatePasswordCommand.class,
            ExitCommand.class, HelpCommand.class, ProjectFindAllCommand.class, ProjectFindOneCommand.class,
            ProjectMergeCommand.class, ProjectInsertCommand.class, ProjectRemoveAllCommand.class, ProjectRemoveCommand.class,
            ProjectUpdateCommand.class, TaskFindAllCommand.class, TaskFindOneCommand.class, TaskMergeCommand.class,
            TaskInsertCommand.class, TaskRemoveAllCommand.class, TaskRemoveCommand.class, TaskUpdateCommand.class
    };

    public static void main(String[] args) {
        BasicConfigurator.configure();
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init(CLASSES);
        bootstrap.start();
    }
}