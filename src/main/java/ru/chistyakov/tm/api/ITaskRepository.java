package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;
import java.util.Date;

public interface ITaskRepository {


    boolean insert(@NotNull String userId, @NotNull String projectId, @NotNull String taskName,
                   @Nullable String descriptionTask, @Nullable Date dateBeginTask, @Nullable Date dateEndTask);

    boolean persist(@NotNull Task task);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String taskId);

    @Nullable
    Collection<Task> findAllByProjectId(@NotNull String projectId);

    boolean update(@NotNull String userId, @NotNull String taskId, @NotNull String projectId, @NotNull String newName,
                   @Nullable String description, @Nullable Date dateBegin, @Nullable Date dateEnd);

    boolean remove(@NotNull String userId, @NotNull String taskId);

    boolean removeByProjectId(@NotNull String userId, @NotNull String projectId);

    void removeAll(@NotNull String userId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId);

    boolean merge(@NotNull Task task);

}
