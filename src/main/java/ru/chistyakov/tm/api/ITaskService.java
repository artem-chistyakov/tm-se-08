package ru.chistyakov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.chistyakov.tm.entity.Task;

import java.util.Collection;

public interface ITaskService {


    boolean insert(@NotNull String userId, @NotNull String projectId, @NotNull String taskName, @Nullable String descriptionTask,
                   @Nullable String dateBeginTask, @Nullable String dateEndTask);

    boolean persist(@NotNull Task task);

    boolean merge(@Nullable Task task);

    @Nullable
    Task findOne(@NotNull String userId, @NotNull String taskId);

    @NotNull
    Collection<Task> findAll(@NotNull String userId);

    boolean update(@NotNull String userId, @NotNull String taskId, @NotNull String projectId,
                   @NotNull String newName, @Nullable String description, @Nullable String dateBegin,
                   @Nullable String dateEnd);

    boolean remove(@NotNull String userId, @NotNull String taskId);

    void removeAll(@NotNull String userId);
}
