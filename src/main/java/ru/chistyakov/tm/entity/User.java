package ru.chistyakov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.chistyakov.tm.enumerate.RoleType;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity {

    @NotNull
    private String login = "";
    @NotNull
    private String password = "";
    @NotNull
    private RoleType roleType = RoleType.ANONIM;

    @Override
    public String toString() {
        return " роль " + this.getRoleType() + " login " + this.getLogin();
    }
}
