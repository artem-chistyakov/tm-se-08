package ru.chistyakov.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractEntity {

    @Nullable
    private String projectId;
    @Nullable
    private String description;
    @Nullable
    private Date dateBeginTask;
    @Nullable
    private Date dateEndTask;
    @NotNull
    private String name = "";
    @Nullable
    private String userId;

    @Override
    public String toString() {
        return "Task{" +
                "projectId='" + projectId + '\'' +
                ", description='" + description + '\'' +
                ", dateBeginTask=" + getDateBeginTask() +
                ", dateEndTask=" + getDateEndTask() +
                ", id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
